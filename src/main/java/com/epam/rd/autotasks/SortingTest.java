package com.epam.rd.autotasks;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class SortingTest {
    Sorting sorting = new Sorting();

    @Test(expected = IllegalArgumentException.class)
    public void testNullCase(){
        sorting.sort(null);
    }

    @Test
    public void testEmptyCase(){
        int[] array = {};

        sorting.sort(array);

        assertArrayEquals(new int[]{}, array);
    }

    @Test
    public void testSingleElementArrayCase() {
        int[] array = {1};

        sorting.sort(array);

        assertArrayEquals(new int[] {1}, array);
    }

    @Test
    public void testSortedArraysCase() {
        int[] array = {0, 0, 1, 2, 2, 3, 4, 5, 7, 9, 10};

        sorting.sort(array);

        assertArrayEquals(new int[] {0, 0, 1, 2, 2, 3, 4, 5, 7, 9, 10}, array);
    }

    @Test
    public void testOtherCases() {
        int[] array = {8, 2, 3, 4, 1, 2, 5, 9, 6, 1, 0};

        sorting.sort(array);

        assertArrayEquals(new int[] {0, 1, 1, 2, 2, 3, 4, 5, 6, 8, 9}, array);
    }
}